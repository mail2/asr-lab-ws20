#pragma once

#include <iterator> // for (only) std::forward_iterator_tag

#include <clean-core/span.hh>

namespace asr
{
/// like std::swap
template <class T>
constexpr void real_swap(T& a, T& b) // no ADL-jacking please
{
    T tmp = static_cast<T&&>(a);
    a = static_cast<T&&>(b);
    b = static_cast<T&&>(tmp);
}

/// STL-compatible iterator with the main intention of sorting two parallel arrays (same length)
/// while only comparing values of the first one
/// (application here: sorting vocabulary hash array [for faster searches], and keeping the string array in sync)
template <class T1, class T2>
struct parallel_iterator
{
    // these are required for std::iterator_traits to work
    using difference_type = std::ptrdiff_t; // value received when subtracting two of these
    using value_type = T1;                  // value that can be "dereferenced" from these
    using pointer = T1*;
    using reference = T1&;
    using iterator_category = std::random_access_iterator_tag;

    parallel_iterator& operator++() noexcept // pre-increment
    {
        ++_data1;
        ++_data2;
        return *this;
    }

    parallel_iterator operator++(int) noexcept // post-increment
    {
        parallel_iterator res = *this;
        ++(*this); // calls the pre-increment op above
        return res;
    }

    T1& operator*() const noexcept { return *_data1; }

    bool operator==(parallel_iterator const& rhs) const noexcept { return _data1 == rhs._data1; }
    bool operator!=(parallel_iterator const& rhs) const noexcept { return _data1 != rhs._data1; }

    inline friend difference_type operator-(parallel_iterator const& lhs, const parallel_iterator const& rhs) { return lhs._data1 - rhs._data1; }


    void swap_from(parallel_iterator& other) noexcept
    {
        real_swap(_data1[0], other._data1[0]);
        real_swap(_data2[0], other._data2[0]);
    }

    explicit constexpr parallel_iterator(T1* begin1, T2* begin2) : _data1(begin1), _data2(begin2) {}

private:
    T1* _data1;
    T2* _data2;
};

template <class T1, class T2>
parallel_iterator<T1, T2> it_parallel_begin(cc::span<T1> arr1, cc::span<T2> arr2)
{
    CC_ASSERT(arr1.size() == arr2.size());
    return parallel_iterator(arr1.begin(), arr2.begin());
}

template <class T1, class T2>
parallel_iterator<T1, T2> it_parallel_end(cc::span<T1> arr1, cc::span<T2> arr2)
{
    CC_ASSERT(arr1.size() == arr2.size());
    return parallel_iterator(arr1.end(), arr2.end());
}

template <class T1, class T2>
void swap(parallel_iterator<T1, T2>& lhs, parallel_iterator<T1, T2>& rhs) noexcept
{
    lhs.swap_from(rhs);
}
}
