#pragma once

#include <clean-core/alloc_array.hh>

namespace asr
{
cc::alloc_array<char> read_file(char const* path, cc::allocator* alloc = cc::system_allocator);
}
