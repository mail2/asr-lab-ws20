#pragma once

#include <cstddef> // size_t
#include <cstdint>

#include <clean-core/span.hh>

namespace asr
{
// ref https://xueyouchao.github.io/2016/11/16/CompileTimeString/
template <size_t N>
constexpr uint64_t stringhash(const char (&str)[N], uint64_t prime = 31, size_t length = N - 1)
{
    return (length <= 1) ? str[0] : (prime * stringhash(str, prime, length - 1) + str[length - 1]);
}

inline uint64_t stringhash_runtime(char const* str, uint64_t prime = 31)
{
    if (str == nullptr)
        return 0;
    uint64_t hash = *str;
    for (; *(str + 1) != 0; str++)
    {
        hash = prime * hash + *(str + 1);
    }
    return hash;
}

inline uint64_t hash_combine(uint64_t a, uint64_t b) { return a * 6364136223846793005ULL + b + 0xda3e39cb94b95bdbULL; }
inline uint64_t hash_combine(uint64_t a, uint64_t b, uint64_t c) { return hash_combine(hash_combine(a, b), c); }

inline uint64_t hash_array_combine(cc::span<uint64_t const> hashes)
{
    CC_CONTRACT(!hashes.empty());

    uint64_t res = hashes[0];
    for (auto i = 1u; i < hashes.size(); ++i)
    {
        res = hash_combine(res, hashes[i]);
    }

    return res;
}
}
