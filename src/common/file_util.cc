#include "file_util.hh"

#include <clean-core/macros.hh>

#include <cstdio>

cc::alloc_array<char> asr::read_file(const char* path, cc::allocator* alloc)
{
#ifdef CC_OS_WINDOWS
    // avoid CRT deprecation warnings on win32
    std::FILE* fp = nullptr;
    errno_t err = ::fopen_s(&fp, path, "rb");
    if (err != 0 || !fp)
        return {};
#else
    std::FILE* fp = std::fopen(path, "rb");
    if (!fp)
        return {};
#endif

    std::fseek(fp, 0, SEEK_END);
    auto res = cc::alloc_array<char>::uninitialized(std::ftell(fp) + 1, alloc);
    std::rewind(fp);
    std::fread(&res[0], 1, res.size() - 1, fp);
    std::fclose(fp);

    res[res.size() - 1] = '\0';
    return res;
}
