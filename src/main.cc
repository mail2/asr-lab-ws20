#include <cstdint>
#include <cstdio>

#include <algorithm> // std::sort
#include <sstream>
#include <string>
#include <unordered_map>
#include <vector>

#include <clean-core/alloc_vector.hh>
#include <clean-core/assert.hh>
#include <clean-core/utility.hh>

#include <task-dispatcher/td.hh>

#include "common/file_util.hh"
#include "common/hashing.hh"
#include "common/parallel_iterator.hh"
#include "container/flat_map.hh"

#define ASR_COUNTOF(_arr_) (sizeof(_arr_) / sizeof(_arr_[0]))

#define ASR_USE_SMALL_FILE 0

#if ASR_USE_SMALL_FILE
#define ASR_CORPUS_FILE "data/sheet1/test"
#else
#define ASR_CORPUS_FILE "data/sheet1/corpus"
#endif

#define ASR_VOCABULARY_FILE "data/sheet1/data_lm_vocabulary.txt"

#define ASR_SKIP_TASK1 1
#define ASR_SKIP_SLOW_NAIVE_TASK3 1

namespace
{
inline uint64_t rdtsc()
{
#if defined(__GNUC__) && !defined(__clang__)
    unsigned int lo, hi;
    __asm__ __volatile__("rdtsc" : "=a"(lo), "=d"(hi));
    return ((uint64_t)hi << 32) | lo;
#else
    return __rdtsc();
#endif
}

struct token_info
{
    unsigned num_occurences = 0;
};

struct ngram_info
{
    unsigned num_occurences = 0;
    unsigned latest_index = 0; // the token index where this trigram occurs _last_ in the corpus
};


struct corpus_statistics
{
    unsigned num_tokens = 0;                                     // amount of tokens (words, punctuation, etc.)
    unsigned num_lines = 0;                                      // amount of sentences (each on a new line)
    unsigned line_length_occurence[1024] = {0};                  // index: line length (in tokens), value: amount of lines with that length
    std::unordered_map<std::string, token_info> token_occurence; // key: token, value: "info" - occurence
};

// turns data = {a, b, c, d} and indices = {3, 2, 0, 1}
// into         {d, c, a, b}
template <class T>
void permute_by_indices(cc::span<T> inout_data, cc::span<unsigned const> indices, cc::allocator* scratch_alloc)
{
    CC_ASSERT(inout_data.size() == indices.size() && "indices do not match data length");

    cc::alloc_array<T> temp_copy;
    if constexpr (std::is_trivially_copyable_v<T>)
    {
        temp_copy = cc::alloc_array<uint64_t>::uninitialized(inout_data.size(), scratch_alloc);
        std::memcpy(temp_copy.data(), inout_data.data(), inout_data.size_bytes());
    }
    else
    {
        temp_copy = cc::alloc_array<T>::defaulted(inout_data.size(), scratch_alloc);
        for (auto i = 0u; i < temp_copy.size(); ++i)
        {
            temp_copy[i] = cc::move(inout_data[i]);
        }
    }

    for (auto i = 0u; i < inout_data.size(); ++i)
    {
        inout_data[i] = cc::move(temp_copy[indices[i]]);
    }
}

struct token_list
{
    cc::alloc_vector<std::string> ordered_tokens;    // ordered tokens, <s> and </s> injected (slow)
    cc::alloc_vector<uint64_t> ordered_token_hashes; // ordered token hashes, <s> and </s> injected (fast)

    void reserve(size_t num)
    {
        ordered_tokens.reserve(num);
        ordered_token_hashes.reserve(num);
    }

    void push_token(char const* token)
    {
        ordered_tokens.push_back(std::string(token)); // we could move here instead but almost all tokens fit into SBO anyway
        ordered_token_hashes.push_back(asr::stringhash_runtime(token));
    }

    void push_token(char const* token, uint64_t hash)
    {
        ordered_tokens.push_back(std::string(token));
        ordered_token_hashes.push_back(hash);
    }

    // sorts hashes to be incrementing over the array, while keeping the string array correctly parallel
    void sort_parallel(cc::allocator* scratch_alloc = cc::system_allocator)
    {
        auto indices_array = cc::alloc_array<unsigned>::uninitialized(ordered_token_hashes.size(), scratch_alloc);
        for (auto i = 0u; i < indices_array.size(); ++i)
        {
            indices_array[i] = i;
        }

        std::sort(indices_array.begin(), indices_array.end(),
                  [&](unsigned i_lhs, unsigned i_rhs) { return ordered_token_hashes[i_lhs] < ordered_token_hashes[i_rhs]; });

        permute_by_indices<uint64_t>(ordered_token_hashes, indices_array, scratch_alloc);
        permute_by_indices<std::string>(ordered_tokens, indices_array, scratch_alloc);

        //        for (auto i = 0u; i < ordered_tokens.size(); ++i)
        //        {
        //            printf("sorted #%u - %zu - %s\n", i, ordered_token_hashes[i], ordered_tokens[i].c_str());
        //        }
        //        fflush(stdout);
    }
};

struct corpus_line_indices
{
    struct line_info
    {
        size_t start;
        size_t length;
    };

    cc::alloc_vector<line_info> line_infos;
};


// takes a corpus string, writes statistics and <s>/</s>-padded token list
void compute_corpus_statistics(char const* corpus, corpus_statistics& out_stats, token_list& out_list)
{
    std::istringstream ss(corpus);

    std::string line;
    line.reserve(512);
    std::string token;
    token.reserve(64);

    // iterate over lines
    while (std::getline(ss, line))
    {
        ++out_stats.num_lines;

        out_list.push_token("<s>");

        // iterate over tokens in line
        unsigned line_length = 0;
        std::istringstream lss(line);
        while (lss >> token)
        {
            ++line_length;

            // operator[] default constructs the value if the key is new,
            // thus num_occurences is initially 0 for each token
            out_stats.token_occurence[token].num_occurences++;

            out_list.push_token(token.c_str());
        }

        CC_ASSERT(line_length < ASR_COUNTOF(out_stats.line_length_occurence) && "line length OOB");
        ++out_stats.line_length_occurence[line_length];
        out_stats.num_tokens += line_length;

        out_list.push_token("</s>");
    }
}

float tokenize_corpus(char const* corpus, cc::span<uint64_t const> vocab_hashes, token_list& out_list, corpus_line_indices& out_line_infos)
{
#define ASR_USE_VOCAB_BINSEARCH 1

    auto f_is_known_vocab = [&](uint64_t token_hash) -> bool {
#if ASR_USE_VOCAB_BINSEARCH
        return std::binary_search(vocab_hashes.begin(), vocab_hashes.end(), token_hash, [](uint64_t lhs, uint64_t rhs) { return lhs < rhs; });
#else
        for (auto const hash : vocab_hashes)
        {
            if (hash == token_hash)
                return true;
        }
        return false;
#endif
    };

    std::istringstream ss(corpus);
    std::string line;
    line.reserve(512);
    std::string token;
    token.reserve(64);

    unsigned num_tokens_total = 0;
    unsigned num_tokens_out_of_vocab = 0;

    // iterate over lines
    while (std::getline(ss, line))
    {
        if ((out_line_infos.line_infos.size() & 0b1111111111111111) == 0)
        {
            printf("[tokenize_corpus] line %zu of corpus\n", out_line_infos.line_infos.size());
            fflush(stdout);
        }

        size_t const first_token_index = out_list.ordered_token_hashes.size();

        out_list.push_token("<s>");
        size_t line_length_in_tokens = 2; // <s>/</s>

        // iterate over tokens in line
        std::istringstream lss(line);
        while (lss >> token)
        {
            ++num_tokens_total;
            ++line_length_in_tokens;
            auto const token_hash = asr::stringhash_runtime(token.c_str());

            if (f_is_known_vocab(token_hash))
            {
                out_list.push_token(token.c_str(), token_hash);
            }
            else
            {
                ++num_tokens_out_of_vocab;
                out_list.push_token("<unk>");
            }
        }
        out_list.push_token("</s>");

        out_line_infos.line_infos.push_back({first_token_index, line_length_in_tokens});
    }

    return num_tokens_out_of_vocab / float(num_tokens_total);
}

void tokenize_vocabulary(char const* vocabulary, token_list& out_list)
{
    std::istringstream ss(vocabulary);
    std::string token;
    token.reserve(64);

    while (ss >> token)
        out_list.push_token(token.c_str());

    out_list.sort_parallel();
}

using token_with_info_t = std::pair<std::string, token_info>;

// takes corpus statistics, returns a vector of token/info pairs, most frequent token at [0]
std::vector<token_with_info_t> get_sorted_frequent_tokens(corpus_statistics const& stats)
{
    std::vector<token_with_info_t> linear_token_infos(stats.token_occurence.begin(), stats.token_occurence.end());
    std::sort(linear_token_infos.begin(), linear_token_infos.end(),
              [](token_with_info_t const& lhs, token_with_info_t const& rhs) { return lhs.second.num_occurences > rhs.second.num_occurences; });

    return linear_token_infos;
}

struct prefixed_count_key
{
    uint64_t prefix_v_hash;
    unsigned occurence;

    constexpr bool operator==(prefixed_count_key const& rhs) const noexcept
    {
        return prefix_v_hash == rhs.prefix_v_hash && occurence == rhs.occurence;
    }
};

struct zeroed_unsigned
{
    unsigned val = 0;
};

struct ngram_statistics
{
    std::unordered_map<uint64_t, ngram_info> ngram_occurence; // key: combined n-gram hash, value: "info" - occurence & last index
    asr::flat_linear_map<unsigned, unsigned> ngram_count_counts; // key: r, value: how many n-grams occur r times - about 35% faster than an STL unordered_map in compute_count_counts on the full corpus
    asr::flat_linear_map<prefixed_count_key, unsigned> prefixed_count_counts; // key: {word hash - v, occurence - r}, value: how many n-grams starting with word v occur r times

    unsigned max_num_ngrams = -1u;
    unsigned max_num_prefixed_ngrams = -1u;

    void compute_count_counts(cc::span<uint64_t const> token_hashes, size_t vocab_size, unsigned n)
    {
        CC_ASSERT(n > 0 && "0-grams illegal");
        CC_ASSERT(prefixed_count_counts._nodes.empty() && "re-ran");

        printf("[compute_count_counts] computing over %zu %u-grams..\n", ngram_occurence.size(), n);
        fflush(stdout);

        auto const t_before = rdtsc();

        // iterate over all unique n-grams
        for (auto const& val : ngram_occurence)
        {
            // unprefixed count-counts
            {
                // get the key in the count-counts map corresponding to all n-grams which also appeared this often
                // default value: 0 (second argument)
                unsigned& occurence_count = ngram_count_counts.get_value(val.second.num_occurences, 0);
                // increment it by one
                occurence_count++;
            }

            // prefixed count-counts
            {
                prefixed_count_key key;
                key.prefix_v_hash = token_hashes[val.second.latest_index]; // prefix: first word
                key.occurence = val.second.num_occurences;

                // get the key in the count-counts map corresponding to all n-grams which also appeared this often
                // default value: 0 (second argument)
                unsigned& occurence_count = prefixed_count_counts.get_value(key, 0);
                // increment it by one
                occurence_count++;
            }
        }

        auto const t_after = rdtsc();
        auto const time = t_after - t_before;

        printf("[compute_count_counts] took ~ %zu ticks\n", time);
        printf("[compute_count_counts] computing zero ccounts for %u-grams..\n", n);
        fflush(stdout);

        // zero-occurence n-grams require special calculations

        // amount of n-grams that occur at least once
        auto num_nonzero_ngrams = ngram_occurence.size();
        // amount of n-grams theoretically existing
        this->max_num_ngrams = std::pow(vocab_size, n);
        auto num_zero_ngrams = unsigned(max_num_ngrams - num_nonzero_ngrams);
        ngram_count_counts.get_value(0) = num_zero_ngrams;

        // zero occurence prefixed n-grams are a little more involved
        // amount of v-prefixed n-grams theoretically existing (for a given v)
        this->max_num_prefixed_ngrams = std::pow(vocab_size, n - 1);

        // this loop iterates over a vector which it modifies at the same time, however the modification is just push_backs
        // thus, fix the loop end right away, do not re-eval .size()
        auto const num_prefixed_ccount_nodes = prefixed_count_counts._nodes.size();
        for (auto i = 0u; i < num_prefixed_ccount_nodes; ++i)
        {
            auto const cc_prefix_node = prefixed_count_counts._nodes[i]; // copy! we modify the vector in this loop body

            prefixed_count_key key;
            key.prefix_v_hash = cc_prefix_node.key.prefix_v_hash;
            key.occurence = 0;

            // subtract the amount of v-prefixed n-grams that have some (nonzero) occurence from the theoretical max
            // trick is to default to max_num_prefixed_ngrams, that way we dont need an additional loop
            prefixed_count_counts.get_value(key, max_num_prefixed_ngrams) -= cc_prefix_node.val;
        }

        printf("[compute_count_counts] done\n");
        fflush(stdout);
    }

    /// returns the amount the given ngram (hash) occured
    unsigned get_count(uint64_t ngram_hash) const
    {
        auto const map_result = ngram_occurence.find(ngram_hash);
        if (map_result == ngram_occurence.end())
            return 0;

        return map_result->second.num_occurences;
    }

    /// returns amount of n-grams which occured r times
    /// N_r(...) (where the arity of N depends on whichever n-grams are contained here)
    unsigned get_count_count(unsigned r) const
    {
        CC_ASSERT(!prefixed_count_counts._nodes.empty() && "didn't run count-counts computation");
        unsigned count = 0;
        ngram_count_counts.search(r, count);
        return count;
    }

    /// returns amount of v-prefixed n-grams which occured r times
    /// N_r(v, ...) (where the arity of N depends on whichever n-grams are contained here)
    unsigned get_prefixed_count_count(uint64_t v_hash, unsigned r) const
    {
        CC_ASSERT(!prefixed_count_counts._nodes.empty() && "didn't run count-counts computation");

        unsigned count = 0;
        bool found = prefixed_count_counts.search(prefixed_count_key{v_hash, r}, count); // writes to count if found, or does nothing

        if (!found && r == 0)
        {
            // not all {v, 0} keys exist in the map because the computation initially doesn't iterate over all vs
            // thus, in this case, not 0 but all possibly existing v-prefixed n-grams occur 0 times,
            // (because none occur any other amount of times)
            return max_num_prefixed_ngrams;
        }

        return count;
    }
};

void compute_ngram_stats(cc::span<uint64_t const> token_hashes, size_t vocab_size, ngram_statistics& out_stats, unsigned n)
{
    CC_ASSERT(n >= 1 && "0-grams illegal");

    // iterate over all token n-tuples
    auto const loop_end = token_hashes.size() - (n - 1);
    for (auto i = 0u; i < loop_end; ++i)
    {
        if ((i & 0x3FFFFF) == 0)
        {
            printf("[compute_ngram_stats] %u-tuple %u of %zu\n", n, i, loop_end);
            fflush(stdout);
        }

        // combine the n next hashes, including i itself (offset i, count n)
        uint64_t const ngram_hash = asr::hash_array_combine(token_hashes.subspan(i, n));
        ngram_info& info = out_stats.ngram_occurence[ngram_hash];

        info.num_occurences++;
        info.latest_index = i;
    }

    out_stats.compute_count_counts(token_hashes, vocab_size, n);
}

struct absolute_discounting_helper
{
    /// calculates p_uni(w)
    double calc_p_uni(uint64_t w_hash) const
    {
        double const t1 = cc::max(double(unigram_stats.get_count(w_hash) - b_uni) / double(N), 0.);
        return t1 + p_uni_bias;
    }

    /// calculates p_bi(w | v)
    double calc_p_bi(uint64_t w_hash, uint64_t v_hash) const
    {
        uint64_t const vw_bigram_hash = asr::hash_combine(v_hash, w_hash); // order relevant
        double const n0_bi_v = double(unigram_stats.get_count(v_hash));    // N_0(v,.)

        double const t1 = cc::max(double(bigram_stats.get_count(vw_bigram_hash) - b_bi) / n0_bi_v, 0.);

        double const p_uni_w = calc_p_uni(w_hash);
        double const t2 = p_uni_w * b_bi * (double(W - bigram_stats.get_prefixed_count_count(v_hash, 0)) / n0_bi_v);
        return t1 + t2;
    }

    absolute_discounting_helper(ngram_statistics const& bi, ngram_statistics const& uni, token_list const& corpus, cc::span<uint64_t const> vocab)
      : bigram_stats(bi),
        unigram_stats(uni), //
        n1_bi(bi.get_count_count(1)),
        n2_bi(bi.get_count_count(2)), //
        n0_uni(uni.get_count_count(0)),
        n1_uni(uni.get_count_count(1)),
        n2_uni(uni.get_count_count(2)), //
        b_bi(n1_bi / double(n1_bi + 2 * n2_bi)),
        b_uni(n1_uni / double(n1_uni + 2 * n2_uni)), //
        N(corpus.ordered_token_hashes.size()),
        W(vocab.size()), //
        p_uni_bias(b_uni * (1.0 / double(W)) * (double(W - n0_uni) / double(N)))
    {
    }

    ngram_statistics const& bigram_stats;
    ngram_statistics const& unigram_stats;

    double n1_bi;
    double n2_bi;

    double n0_uni;
    double n1_uni;
    double n2_uni;

    double b_bi;
    double b_uni;

    size_t N;
    size_t W;

    // second summand of p_uni(w)
    double p_uni_bias;
};

void task4(token_list const& corpus, cc::span<uint64_t const> vocab, ngram_statistics const& bigram_stats, ngram_statistics const& unigram_stats)
{
    absolute_discounting_helper discounting(bigram_stats, unigram_stats, corpus, vocab);

    printf("4. - working on N = %zu tokens in the corpus, W = %zu words in the vocabulary\n", discounting.N, discounting.W);
    printf("4. - discounts: b_bi = %.4f, b_uni = %.4f\n", discounting.b_bi, discounting.b_uni);

    // test if the probabilities sum up to one
    // pick any suitable v from the corpus:

    uint64_t test_v;
    unsigned test_v_i;
    bool found_suitable_v = false;
    for (auto i = corpus.ordered_token_hashes.size() / 4; i < corpus.ordered_token_hashes.size(); ++i)
    {
        // we require one that appears in the vocabulary and thus has N(v) > 0
        auto const v = corpus.ordered_token_hashes[i];
        if (unigram_stats.get_count(v) > 0)
        {
            test_v = v;
            test_v_i = i;
            found_suitable_v = true;
            break;
        }
    }

    CC_ASSERT(found_suitable_v && "programmer error - never found a testing v with N(v) > 0");

    double sum_p_uni = 0.0;
    double sum_p_bi = 0.0;
    for (uint64_t const vocab_w : vocab)
    {
        sum_p_uni += discounting.calc_p_uni(vocab_w);
        sum_p_bi += discounting.calc_p_bi(vocab_w, test_v);
    }

    printf("4. - summing p_uni(w) over all w in the vocabulary yields %f which should be close to 1.0 barring numerical issues\n", sum_p_uni);
    printf("4. - summing p_bi(w | v) over all w and a specific v (\"%s\") yields %f\n", corpus.ordered_tokens[test_v_i].c_str(), sum_p_bi);
}

void task5(token_list const& corpus, corpus_line_indices const& corpus_lines, cc::span<uint64_t const> vocab, ngram_statistics const& bigram_stats, ngram_statistics const& unigram_stats)
{
    absolute_discounting_helper discounting(bigram_stats, unigram_stats, corpus, vocab);

    // -- compute log-likelihood and sum over N_k --

    struct alignas(64) per_batch_results // pad to L1 cacheline
    {
        double log_likelihood = 0.0;
        unsigned perplexity_scale_denom = 0; // sum over (N_k + 1) for all lines k
    };

    // partition the lines into <#logical cores> * 4 batches
    auto const n = corpus_lines.line_infos.size();
    auto const max_num_batches = td::system::num_logical_cores() * 4;
    auto batch_results = cc::alloc_array<per_batch_results>::defaulted(max_num_batches);

    td::sync s;
    auto const num_batches = td::submit_batched_n(
        s,
        [&batch_results, &corpus_lines, &discounting, &corpus](unsigned start, unsigned end, unsigned batch) {
            double log_likelihood = 0.0;
            unsigned perplexity_scale_denom = 0; // sum over (N_k + 1) for all lines k

            // iterate over the range of this batch
            for (auto i = start; i < end; ++i)
            {
                auto const& line = corpus_lines.line_infos[i];
                perplexity_scale_denom += line.length - 1; // originally N_k + 1, line length includes <s>/</s>, N_k excludes them

                for (auto i = 1u; i < line.length; ++i) // skip the first token
                {
                    auto const token_i = line.start + i;
                    log_likelihood += std::log(discounting.calc_p_bi(corpus.ordered_token_hashes[token_i], corpus.ordered_token_hashes[token_i - 1]));
                }
            }

            // write results into batch_results array
            batch_results[batch].log_likelihood = log_likelihood;
            batch_results[batch].perplexity_scale_denom = perplexity_scale_denom;
        },
        n, max_num_batches);

    printf("5. - computing language model perplexity on corpus of %zu tokens\n", corpus.ordered_token_hashes.size());
    printf("5. - %zu lines via %u batches on %u threads\n", n, num_batches, td::get_current_num_threads());
    fflush(stdout);
    td::wait_for(s);

    // sum over the per-batch results serially
    double log_likelihood = 0.0;
    unsigned perplexity_scale_denom = 0; // sum over (N_k + 1) for all lines k

    for (auto i = 0u; i < num_batches; ++i)
    {
        auto const& batch_res = batch_results[i];
        log_likelihood += batch_res.log_likelihood;
        perplexity_scale_denom += batch_res.perplexity_scale_denom;
    }

    double perplexity = std::exp((-1.0 / double(perplexity_scale_denom)) * log_likelihood);

    printf("5. - computed log-likelihood LL = %f\n", log_likelihood);
    printf("5. - computed perplexity PP = %f\n", perplexity);
    printf("5. - [sum over N_k + 1] = %u\n", perplexity_scale_denom);
}

struct trigram_postfixes
{
    uint64_t hash_b;
    uint64_t hash_c;
};

// get (non-unique) postfixes of all trigrams
cc::alloc_vector<trigram_postfixes> get_trigram_postfixes(cc::span<uint64_t const> token_hashes, ngram_statistics const& stats)
{
    cc::alloc_vector<trigram_postfixes> res;
    res.reserve(stats.ngram_occurence.size());
    for (auto const& trigram : stats.ngram_occurence)
    {
        auto const first_token_i = trigram.second.latest_index;
        res.push_back(trigram_postfixes{token_hashes[first_token_i + 1], token_hashes[first_token_i + 2]});
    }

    return res;
}

void compute_summed_sub_trigram_stats(cc::span<trigram_postfixes const> postfixes,
                                      cc::span<uint64_t const> vocab_hashes,
                                      ngram_statistics const& prev_trigrams,
                                      ngram_statistics& out_stats_bigrams,
                                      ngram_statistics& out_stats_unigrams)
{
    for (trigram_postfixes const& postfix : postfixes)
    {
        unsigned num_postfix_occurence = 0u;
        unsigned latest_trigram_occurence = unsigned(-1);

        // inner sum over u, just like in the formula
        for (uint64_t const word_hash_u : vocab_hashes)
        {
            uint64_t const combined_trigram_hash = asr::hash_combine(word_hash_u, postfix.hash_b, postfix.hash_c);

            // search in prev_trigrams for this hash, if found, add its occurences to the bigram occurences
            auto const map_result = prev_trigrams.ngram_occurence.find(combined_trigram_hash);
            if (map_result != prev_trigrams.ngram_occurence.end())
            {
                num_postfix_occurence += map_result->second.num_occurences;
                latest_trigram_occurence = map_result->second.latest_index;
            }
        }

        if (num_postfix_occurence == 0u)
            continue;

        // by nature of the loop, we do not repeat bigrams (assuming vocabulary entries are unique)
        {
            uint64_t const bigram_hash = asr::hash_combine(postfix.hash_b, postfix.hash_c);
            ngram_info& info = out_stats_bigrams.ngram_occurence[bigram_hash];

            CC_ASSERT(latest_trigram_occurence != unsigned(-1) && "programmer error");
            info.num_occurences += num_postfix_occurence; // += just to make sure
            info.latest_index = latest_trigram_occurence;
        }

        {
            // unigrams are repeated
            uint64_t const unigram_hash = postfix.hash_c;
            ngram_info& info = out_stats_unigrams.ngram_occurence[unigram_hash];

            CC_ASSERT(latest_trigram_occurence != unsigned(-1) && "programmer error");
            info.num_occurences += num_postfix_occurence;
            info.latest_index = latest_trigram_occurence;
        }
    }
}

using ngram_with_info_t = std::pair<uint64_t, ngram_info>;

// takes corpus statistics, returns a vector of token/info pairs, most frequent token at [0]
std::vector<ngram_with_info_t> get_sorted_frequent_ngrams(ngram_statistics const& stats)
{
    std::vector<ngram_with_info_t> linear_infos(stats.ngram_occurence.begin(), stats.ngram_occurence.end());
    std::sort(linear_infos.begin(), linear_infos.end(),
              [](ngram_with_info_t const& lhs, ngram_with_info_t const& rhs) { return lhs.second.num_occurences > rhs.second.num_occurences; });

    return linear_infos;
}

}

int main()
{
    td::launch([&]() {
        printf("loading corpus file at %s..\n", ASR_CORPUS_FILE);
        fflush(stdout);
        auto const corpus_contents = asr::read_file(ASR_CORPUS_FILE);
        CC_ASSERT(!corpus_contents.empty() && "failed to open corpus file, check the working directory");
        printf("loaded corpus - %zu chars\n", corpus_contents.size());
        auto const num_tokens_estimate = corpus_contents.size() / 4;

#if ASR_SKIP_TASK1
        printf("1. - SKIPPED\n");
        printf("1. - (define ASR_SKIP_TASK1 to 0 to prevent skipping) \n");
#else
        {
            printf("computing statistics..\n");
            fflush(stdout);

            corpus_statistics corpus_stats;
            token_list corpus_tokens;

            // tokenize and hash corpus, compute stats
            corpus_stats.token_occurence.reserve(num_tokens_estimate);
            corpus_tokens.reserve(num_tokens_estimate);
            compute_corpus_statistics(corpus_contents.data(), corpus_stats, corpus_tokens);

            // 1.
            {
                // 1. a) and b)
                printf("1. a), b): corpus contains %u tokens and %u lines (sentences)\n", corpus_stats.num_tokens, corpus_stats.num_lines);

                // 1. c), TODO: plotting
                printf("1. c): line length occurences: [<line length>: #<amount of lines with that length>]\n");
                for (auto i = 0u; i < ASR_COUNTOF(corpus_stats.line_length_occurence); ++i)
                {
                    auto const occurence = corpus_stats.line_length_occurence[i];
                    if (occurence == 0)
                        continue;

                    printf(" [%u: #%u]", i, occurence);
                }
                printf("\n");
            }

            // 2. a) + b) + c)
            {
                // 2. a)
                auto const frequent_tokens = get_sorted_frequent_tokens(corpus_stats);
                for (auto i = 0u; i < frequent_tokens.size() && i < 10u; ++i)
                {
                    printf("2. a): most frequent word #%u: %s (%u times)\n", i + 1, frequent_tokens[i].first.c_str(), frequent_tokens[i].second.num_occurences);
                }

                printf("computing trigram frequencies..\n");
                fflush(stdout);

                ngram_statistics trigram_stats;
                compute_ngram_stats(corpus_tokens.ordered_token_hashes, 9999999, trigram_stats,
                                    3); // special case where no vocabulary is present, second argument won't make a difference here

                // 2. b)
                auto const frequent_trigrams = get_sorted_frequent_ngrams(trigram_stats);
                printf("2. b): %zu trigrams in total\n", frequent_trigrams.size());
                for (auto i = 0u; i < frequent_trigrams.size() && i < 10u; ++i)
                {
                    auto const& info = frequent_trigrams[i].second;

                    auto const first_index = info.latest_index;

                    printf("2. b): most frequent trigram #%u: %s %s %s (%u times)\n", i + 1, //
                           corpus_tokens.ordered_tokens[first_index].c_str(),                // 0
                           corpus_tokens.ordered_tokens[first_index + 1].c_str(),            // 1
                           corpus_tokens.ordered_tokens[first_index + 2].c_str(),            // 2
                           info.num_occurences);
                }

                // 2. c) TODO: plotting
                printf("2. c): trigram count-counts: [<N>: #<amount of trigrams that occur N times>]\n");
                for (auto const& ccount_node : trigram_stats.ngram_count_counts._nodes)
                {
                    printf(" [%u: #%u]", ccount_node.key, ccount_node.val);
                }
                printf("\n");
            }
        }
#endif

        token_list vocab_tokens;
        {
            printf("loading vocabulary file at %s..\n", ASR_VOCABULARY_FILE);
            fflush(stdout);
            auto const vocab_contents = asr::read_file(ASR_VOCABULARY_FILE);
            CC_ASSERT(!vocab_contents.empty() && "failed to open vocabulary file, check the working directory");
            printf("loaded vocabulary - %zu chars\n", vocab_contents.size());

            // tokenize and hash vocabulary
            printf("parsing vocabulary..\n");
            fflush(stdout);
            vocab_tokens.reserve(vocab_contents.size() / 4);
            tokenize_vocabulary(vocab_contents.data(), vocab_tokens);

            printf("parsed vocabulary - %zu words\n", vocab_tokens.ordered_token_hashes.size());
        }

        // re-compute corpus tokens over the vocabulary
        printf("recomputing corpus over vocabulary..\n");
        fflush(stdout);
        token_list corpus_tokens_over_vocab;
        corpus_line_indices corpus_lines;
        corpus_tokens_over_vocab.reserve(num_tokens_estimate);
        corpus_lines.line_infos.reserve(num_tokens_estimate / 20);
        float const oov_rate = tokenize_corpus(corpus_contents.data(), vocab_tokens.ordered_token_hashes, corpus_tokens_over_vocab, corpus_lines);

        // re-do 2. b) and c)
        ngram_statistics trigram_stats_over_vocab;
        compute_ngram_stats(corpus_tokens_over_vocab.ordered_token_hashes, vocab_tokens.ordered_token_hashes.size(), trigram_stats_over_vocab, 3);

        // 2. d) + e)
        {
            auto const frequent_trigrams_over_vocab = get_sorted_frequent_ngrams(trigram_stats_over_vocab);

            printf("2. d): %zu trigrams in total (over vocabulary)\n", frequent_trigrams_over_vocab.size());
            printf("2. d): most frequent trigrams (over vocabulary): \n");
            for (auto i = 0u; i < frequent_trigrams_over_vocab.size() && i < 10u; ++i)
            {
                auto const& info = frequent_trigrams_over_vocab[i].second;
                auto const first_index = info.latest_index;

                printf("2. d): most frequent trigram #%u: %s %s %s (%u times)\n", i + 1, //
                       corpus_tokens_over_vocab.ordered_tokens[first_index].c_str(),     // 0
                       corpus_tokens_over_vocab.ordered_tokens[first_index + 1].c_str(), // 1
                       corpus_tokens_over_vocab.ordered_tokens[first_index + 2].c_str(), // 2
                       info.num_occurences);
            }

            // 2. c) TODO: plotting
            printf("2. d): trigram count-counts: [<N>: #<amount of trigrams that occur N times>]\n");
            for (auto const& ccount_node : trigram_stats_over_vocab.ngram_count_counts._nodes)
            {
                printf(" [%u: #%u]", ccount_node.key, ccount_node.val);
            }
            printf("\n");

            printf("2. e) out-of-vocabulary (OOV) rate: %.2f %%\n", oov_rate * 100.f);
        }

        // 3. + 4.
        {
            // 3.
            ngram_statistics bigram_stats;
            ngram_statistics unigram_stats;

            // first - "naive" summing over already extracted trigrams
            {
#if ASR_SKIP_SLOW_NAIVE_TASK3
                printf("3. - computing naive sum over (vocab) trigrams SKIPPED\n");
                printf("3. - (define ASR_SKIP_SLOW_NAIVE_TASK3 to 0 to prevent skipping) \n");
                // prevent unused warnings
                (void)&compute_summed_sub_trigram_stats;
                (void)&get_trigram_postfixes;
#else
                printf("3. - computing naive sum over (vocab) trigrams to obtain bigram and unigram frequencies..\n");
                fflush(stdout);

                auto const trigram_postfixes = get_trigram_postfixes(corpus_tokens_over_vocab.ordered_token_hashes, trigram_stats_over_vocab);

                ngram_statistics bigram_stats_naive;
                ngram_statistics unigram_stats_naive;

                printf("3. - naive sum bigrams/unigrams - looping %zu x %zu times\n", trigram_postfixes.size(), vocab_tokens.ordered_token_hashes.size());
                fflush(stdout);

                compute_summed_sub_trigram_stats(trigram_postfixes, vocab_tokens.ordered_token_hashes, trigram_stats_over_vocab, bigram_stats_naive,
                                                 unigram_stats_naive);


                auto const bigram_frequency = get_sorted_frequent_ngrams(bigram_stats_naive);
                auto const unigram_frequency = get_sorted_frequent_ngrams(unigram_stats_naive);

                printf("3. naive summed bigrams: got %zu bigrams in total\n", bigram_frequency.size());
                printf("3. naive summed unigrams: got %zu unigrams in total\n", unigram_frequency.size());
#endif
            } // second - recompute
            {
                printf("3. - recomputing bigrams and unigrams..\n");
                fflush(stdout);

                compute_ngram_stats(corpus_tokens_over_vocab.ordered_token_hashes, vocab_tokens.ordered_token_hashes.size(), bigram_stats, 2);
                compute_ngram_stats(corpus_tokens_over_vocab.ordered_token_hashes, vocab_tokens.ordered_token_hashes.size(), unigram_stats, 1);

                auto const bigram_frequency = get_sorted_frequent_ngrams(bigram_stats);
                auto const unigram_frequency = get_sorted_frequent_ngrams(unigram_stats);

                printf("3. recomputed bigrams: got %zu bigrams in total\n", bigram_frequency.size());
                printf("3. recomputed unigrams: got %zu unigrams in total\n", unigram_frequency.size());
            }

            // 4.
            task4(corpus_tokens_over_vocab, vocab_tokens.ordered_token_hashes, bigram_stats, unigram_stats);

            // 5.
            task5(corpus_tokens_over_vocab, corpus_lines, vocab_tokens.ordered_token_hashes, bigram_stats, unigram_stats);

            printf("- End of Sheet 01 -\n");
        }
    });
}
