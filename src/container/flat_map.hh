#pragma once

#include <clean-core/capped_vector.hh>
#include <clean-core/vector.hh>

namespace asr
{
/// very simple flat associative container
template <class KeyT, class ValT>
struct flat_linear_map
{
public:
    flat_linear_map() = default;
    flat_linear_map(size_t reserve_size) { _nodes.reserve(reserve_size); }

    [[nodiscard]] ValT& get_value(KeyT const& key, ValT const& default_val = ValT{})
    {
        for (auto& node : _nodes)
        {
            if (node.key == key)
                return node.val;
        }

        auto& new_node = _nodes.emplace_back(key, default_val);
        return new_node.val;
    }

    /// get_value, but must not miss - never adds a new node to the map
    [[nodiscard]] ValT& get_value_no_miss(KeyT const& key)
    {
        for (auto& node : _nodes)
        {
            if (node.key == key)
                return node.val;
        }

        CC_ASSERT(false && "failed to find key in no_miss variant");
        return _nodes[0].val; // dummy return
    }

    [[nodiscard]] bool contains(KeyT const& key) const
    {
        for (auto const& node : _nodes)
            if (node.key == key)
                return true;

        return false;
    }

    bool search(KeyT const& key, ValT& out_value) const
    {
        for (auto const& node : _nodes)
            if (node.key == key)
            {
                out_value = node.val;
                return true;
            }

        return false;
    }

    void clear() { _nodes.clear(); }
    void shrink_to_fit() { _nodes.shrink_to_fit(); }
    void reserve(size_t size) { _nodes.reserve(size); }

    struct map_node
    {
        KeyT key;
        ValT val;

        map_node(KeyT const& key, ValT const& val) : key(key), val(val) {}
    };

    cc::vector<map_node> _nodes;
};

/// very simple flat associative container, fixed-size (stack only)
template <class KeyT, class ValT, size_t N>
struct capped_flat_map
{
public:
    [[nodiscard]] ValT& get_value(KeyT const& key, ValT const& default_val = ValT{})
    {
        for (auto& node : _nodes)
        {
            if (node.key == key)
                return node.val;
        }

        auto& new_node = _nodes.emplace_back(key, default_val);
        return new_node.val;
    }

    [[nodiscard]] bool contains(KeyT const& key) const
    {
        for (auto const& node : _nodes)
            if (node.key == key)
                return true;

        return false;
    }

    void clear() { _nodes.clear(); }

    struct map_node
    {
        KeyT key;
        ValT val;

        map_node(KeyT const& key, ValT const& val) : key(key), val(val) {}
    };

    cc::capped_vector<map_node, N> _nodes;
};
}
